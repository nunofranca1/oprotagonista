<?php

use Illuminate\Support\Facades\Route;
use Spatie\Analytics\Period;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);

Route::middleware('auth')->group(function () {

    Route::get('/admin', 'Admin\PostsController@create')->name('admin.login');
    Route::get('/admin/postagem/novo', 'Admin\PostsController@create')->name('postagem.create');
    Route::post('/admin/postagem/novo', 'Admin\PostsController@store')->name('postagem.store');
    Route::get('/admin/postagem', 'Admin\PostsController@index')->name('admin.index');
    Route::get('/admin/postagem/comentarios', 'Admin\CommentsController@index')->name('postagem.index');
    Route::put('/admin/postagem/{id}', 'Admin\PostsController@update')->name('postagem.update');
    Route::get('/admin/postagem/{id}/comentarios', 'Admin\CommentsController@show')->name('list.comments');

    Route::post('/admin/comentario/{id}/edit', 'Admin\CommentsController@update')->name('comment.update');



    Route::get('/admin/postagem/agendada', 'Admin\PostsController@agendada')->name('postagem.agendada');
    Route::get('/admin/postagem/publicada', 'Admin\PostsController@publicada')->name('postagem.publicada');
    Route::get('/admin/postagem/cronJob', 'Admin\PostsController@cronJob')->name('postagem.cronJob');
    Route::get('/admin/postagem/{id}/edit', 'Admin\PostsController@edit')->name('postagem.edit');
    Route::delete('/admin/postagem/{id}', 'Admin\PostsController@destroy')->name('postagem.delete');


    Route::get('/admin/categoria/novo', 'Admin\CategoryController@create')->name('categoria.create');
    Route::get('/admin/categoria', 'Admin\CategoryController@index')->name('categoria.index');
    Route::post('/admin/categoria/novo', 'Admin\CategoryController@store')->name('categoria.store');

    Route::get('/admin/anuncios', 'Admin\AnunciosController@index')->name('anuncio.index');
    Route::get('/admin/anuncios/novo', 'Admin\AnunciosController@create')->name('anuncio.novo');
    Route::post('/admin/anuncios/novo', 'Admin\AnunciosController@store')->name('anuncio.novo.store');
    Route::delete('/admin/anuncios/destroy/{id}', 'Admin\AnunciosController@destroy')->name('anuncio.delete');
    Route::put('/admin/anuncios/update/{id}', 'Admin\AnunciosController@update')->name('anuncio.update');

    Route::get('/admin/configs', 'Admin\ConfigsController@configs')->name('configs.configs');
    Route::post('/admin/configs', 'Admin\ConfigsController@store_configs')->name('configs.store');

    Route::get('/admin/tv', 'Admin\MoviesController@index')->name('tv.index');
    Route::post('/admin/tv', 'Admin\MoviesController@store')->name('tv.store');
    Route::delete('/admin/tv/{id}', 'Admin\MoviesController@destroy')->name('tv.delete');
    Route::put('/admin/tv/{id}', 'Admin\MoviesController@update')->name('tv.update');
});




Route::get('/', 'SiteController@index')->name('home.index');



Route::get('/noticia/{noticia}', 'SiteController@show')->name('noticia.show');
Route::post('/noticia/comment/{link}', 'Admin\CommentsController@store')->name('post.comments');
Route::get('/search', 'SiteController@search')->name('noticias.search');

Route::post('/contato', 'Admin\ContactController@store')->name('contato.store');


Route::get('/home', 'Admin\PostsController@index')->name('home');




//Route::get('/{ano}/{mes}/{titulo}', 'SiteController@blogger')->name('noticia.blogger');


//Route::get('/ajuste', 'Manutencao\AddTableImgController@index')->name('table.image');
