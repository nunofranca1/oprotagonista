$(document).ready(function (){

    $("#formulario").submit(function (event){
        event.preventDefault()

        let rota = '/contato'
        $.ajax({
            type: "POST",
            //dataType: "json",
            url: rota,
            data: $("#formulario").serialize(),
            beforeSend: function (){
                let timerInterval
                Swal.fire({
                    title: 'Sua mensagem está sendo enviada!',
                    html: 'Tempo previsto <b></b> milisecundos.',
                    timer: 2000,
                    timerProgressBar: true,
                    didOpen: () => {
                        Swal.showLoading()
                        timerInterval = setInterval(() => {
                            const content = Swal.getContent()
                            if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                    b.textContent = Swal.getTimerLeft()
                                }
                            }
                        }, 100)
                    },
                    willClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then((result) => {
                    /* Read more about handling dismissals below */
                    if (result.dismiss === Swal.DismissReason.timer) {
                        console.log('I was closed by the timer')
                    }
                })
            },
            success: function (data){
                if(data = 1){
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Mensagem enviada com sucesso',
                        showConfirmButton: false,
                        timer: 1500
                    })

                    $("input[type=text]").val('')
                    $("input[type=email]").val('')
                    $("textarea").val('')
                }else{
                    alert('Mensagem não enviado')
                }

            }


        })

    })
    $("#cadastrar").click(function (event){
        let timerInterval
        Swal.fire({

            html: 'Aguarde. Estamos fazendo todos os ajustes necessários na postagem! Em instantes estará tudo pronto.',
            timer: 7000,

            didOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                    const content = Swal.getContent()
                    if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                            b.textContent = Swal.getTimerLeft()
                        }
                    }
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
                console.log('I was closed by the timer')
            }
        })
    });



})
