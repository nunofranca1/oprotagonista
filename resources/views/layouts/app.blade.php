<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'OProtagonista') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
    :root{
    --cor-logo-primary: #14a138;
    --cor-primary-low-opacity: rgba(121, 145, 165, 0.377);
    --cor-logo-secundary: #c70023;

}


.form-login{
    height: 340px;
    width: 95%;
    min-width: 280px;
    max-width: 450px;
    padding: 1%;

    border-radius: 10px;
    background-color: white;

    box-shadow: 2px 2px 15px 2px var(--cor-primary-low-opacity);
    -webkit-box-shadow: 2px 2px 15px 2px var(--cor-primary-low-opacity);

}
.form-login .title-login{
    width: 100%;
    height: 80px;
}


#form-login{
  width: 100%;

}
#icon-login{
    color: var(--cor-logo-primary);
    background-color: var(--cor-primary-low-opacity);
    font-size: 35px;
    padding: 5px;
    border-right: transparent;
    border-top-right-radius: 0px;
    border-top-left-radius: 5px;
    border-bottom-right-radius: 0px;
    border-bottom-left-radius: 5px;
    border-left: 2px solid var(--cor-primary-low-opacity);
    border-bottom: 2px solid var(--cor-primary-low-opacity);
    border-top: 2px solid var(--cor-primary-low-opacity);

}
.form-login .form-group input{
    height: 30px;
    background-color: white !important;
    display: block;
    width: 100%;
    height: 35px;
    border: none;
    border-radius: 5px;
    border: 2px solid var(--cor-primary-low-opacity);
    transition: all 1s;
    max-width: 310px;

    border-top-left-radius: 0px;
    border-top-right-radius: 5px;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 5px;
    border-left: 2px solid var(--cor-primary-low-opacity);
    border-bottom: 2px solid var(--cor-primary-low-opacity);
    border-top: 2px solid var(--cor-primary-low-opacity);
}
.form-login .form-group input:focus{
    outline: none;
    box-shadow: 0 0 0 0;
    outline: 0;
    background-color: transparent !important;
}

#form-login label{
    margin-left: 0px;
    bottom: 28px;
    transition: transform .2s ease-in-out;
    font-weight: 700;
    width: 90%;
}

.form-login .form-group label span{
    color: var(--cor-logo-secundary);
}

.form-login .btn-entrar{
    color: white !important;
    background-color: var(--cor-logo-primary) !important;
    font-weight: 700;
    letter-spacing: 2px;
    font-size: 18px;
    text-transform: uppercase;
    border: none;
    outline: none;
    height: 40px;
    width: 90%;
    max-width: 350px;

    margin-left: 15px;
}
@media  (min-width: 450px){
    #form-login label{
        margin-left: 29px;
    }
    .form-login .btn-entrar{
        margin-left: 45px;
    }
}
@media  (min-width: 1000px){
    #form-login label{
        margin-left: 25px;
    }
    .form-login .btn-entrar{
        margin-left: 35px;
    }
}

</style>

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
{{--                <a class="navbar-brand" href="{{ url('/') }}">--}}
{{--                    {{ config('app.name', 'OProtagonista') }}--}}
{{--                </a>--}}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
{{--                            </li>--}}
{{--                            @if (Route::has('register'))--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
{{--                                </li>--}}
{{--                            @endif--}}
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
