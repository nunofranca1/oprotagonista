@extends('adminlte::page')

@section('title', 'Dashboard')
<link rel="stylesheet" href="/assets/css/style.css">
@section('content_header')

@stop

@section('content')

@stop

@section('css')


@stop

@section('js')

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdnjs.com/libraries/popper.js"></script>
    <script src="https://cdn.ckeditor.com/4.15.0/full/ckeditor.js"></script>
    <script src="{{ asset('assets/js/script.js') }}"></script>
    <script src="{{ asset('assets/site/js/libs/ckEditor.js') }}"></script>

    <script src="{{ asset('assets/site/js/script-geral-custom.js') }}"></script>
    <script src="{{ asset('assets/admin/js/customizacao.js') }}"></script>



@stop
