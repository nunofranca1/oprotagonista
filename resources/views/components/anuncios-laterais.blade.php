<div>
    <h2 class="titulo mt-2"><span>Publicidades</span></h2>
    @if(Request::segment(1) == "noticia")
        <div class="d-none d-sm-none d-md-block">
            <div class="publicidades-lateral-lista">
                @foreach($anuncios_laterais as $anuncio)

                        @if($anuncio->tamanho == "333x333")
                            @if(!empty($anuncio->link))
                                <a href="{{$anuncio->link}}" target="_blank">
                                    <img src='{{url("storage/{$anuncio->img}")}}'>
                                </a>
                            @else
                                <img src='{{url("storage/{$anuncio->img}")}}'>
                            @endif
                        @endif

                @endforeach
            </div>

        </div>

    @else
        <div class="d-none d-sm-none d-md-block">
            <div class="publicidades-lateral-lista">
                @foreach($anuncios_laterais as $anuncio)

                    @if($anuncio->tamanho == "333x333")
                        @if(!empty($anuncio->link))
                            <a href="{{$anuncio->link}}" target="_blank">
                                <img src='{{url("storage/{$anuncio->img}")}}'>
                            </a>
                        @else
                            <img src='{{url("storage/{$anuncio->img}")}}'>
                        @endif
                    @endif

                @endforeach
            </div>

        </div>
    @endif

    <div class="d-block d-sm-block d-md-none">
        <div class="publicidades-lateral-lista">
            @foreach($anuncios_laterais as $anuncio)

                @if($anuncio->tamanho == "333x333")
                    @if(!empty($anuncio->link))
                        <a href="{{$anuncio->link}}" target="_blank">
                            <img src='{{url("storage/{$anuncio->img}")}}'>
                        </a>
                    @else
                        <img src='{{url("storage/{$anuncio->img}")}}'>
                    @endif
                @endif

            @endforeach
        </div>
    </div>
</div>