<div>
    <h2 class="titulo">Leia <span>mais</span></h2>
    <div class="">
        <div class="row">
            @foreach($posts as $post)

                <div class="col-12 col-lg-4 col-md-4 col-xl-4">
                    <span class="text-dark ">{{$post->category->name}}  - O Protagonista</span>
                    <div class="info-noticia text-white text-secondary atulizacao">
                        Atualizado em
                        {{ \Carbon\Carbon::parse($post->updated_at)->format('d-m-Y  H:i')}}
                    </div>
                    <a href="{{ route('noticia.show', $post->link) }}" class="link_materia">
                        <h2 class="title_h2 ">{{ $post->title }}</h2>
                    </a>
                    <hr>
                </div>

            @endforeach

        </div>
    </div>
</div>