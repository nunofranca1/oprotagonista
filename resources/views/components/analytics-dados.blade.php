
<div>
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">

                        <div class="col">
                            <h6 class=" text-muted mb-0">
                                Views: últimos 30 dias
                                <small>
                                    {{\Carbon\Carbon::parse($visits30dias['query']['modelData']['start-date'])->format('d/m/Y')}} até
                                    {{\Carbon\Carbon::parse($visits30dias['query']['modelData']['end-date'])->format('d/m/Y')}}
                                </small>

                            </h6>

                            <span class="h6 font-weight-bold mb-0">{{$visits30dias['totalsForAllResults']['ga:pageViews']}}</span> - Visitas
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                <i class="ni ni-active-40"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h6 class=" text-muted mb-0">Views: últimos 7 dias
                                <small>
                                    {{\Carbon\Carbon::parse($visits7dias['query']['modelData']['start-date'])->format('d/m/Y')}} até
                                    {{\Carbon\Carbon::parse($visits7dias['query']['modelData']['end-date'])->format('d/m/Y')}}
                                </small>
                            </h6>

                            <span class="h6 font-weight-bold mb-0">{{$visits7dias['totalsForAllResults']['ga:pageViews']}} </span> - Visitas
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                <i class="ni ni-active-40"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h6 class=" text-muted mb-0">Views: Ontem
                                <br>
                                <small>
                                    {{\Carbon\Carbon::parse($ontem['query']['modelData']['start-date'])->format('d/m/Y')}}
                                </small>
                            </h6>

                            <span class="h6 font-weight-bold mb-0">{{$ontem['totalsForAllResults']['ga:pageViews']}} </span> - Visitas
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                <i class="ni ni-active-40"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>
