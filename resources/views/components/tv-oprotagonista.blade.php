<div>
    <div class="row">
        <div class="col-12 col-lg-12 col-md-12 col-xl-12">
            <h2 class="titulo">TV <span>O Protagonista</span></h2>
        </div>
        @foreach($movies as $movie)

            <div class="col-12 col-sm-12 col-lg-12 col-md-12 col-xl-12">
                <h6 class="text-danger text-left mb-2">
                    <a href="{{ is_null($movie->post) ? "#" : route('noticia.show', $movie->post->link)  }}">
                        <span class="text-danger">
                            {{ $movie->title ? $movie->title : $movie->post->title}}
                        </span>
                        <span class="text-dark">
                            {{ is_null($movie->post) ? "" : ' - Ler mais' }}

                        </span>
                    </a>
                </h6>
                <div class="embed-responsive img-thumbnail rounded float-start embed-responsive-16by9">
                    <div class="tv ">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $movie->link }}?rel=0&modestbranding=1&showinfo=0" allowfullscreen></iframe>
                    </div>

                </div>
            </div>
        @endforeach
    </div>

    <hr>
</div>