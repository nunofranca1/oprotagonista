//aquivo de customizacao
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".newllester").on('submit', function (e) {
        e.preventDefault();
        let url = '/contato'
        let data = {
            'name': $(this).find('input[name="name"]').val(),
            'email': $(this).find('input[name="email"]').val(),
        }
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            //dataType: 'json',
            success: function () {
                Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Obrigado por se cadastrar em nossa newlestter',
                    showConfirmButton: false,
                    timer: 1500
                })
                $('.newllester').each(function () {
                    this.reset();
                });
            }
        })

    })


    jQuery('.toggle-nav').click(function (e) {
        jQuery(this).toggleClass('active');
        jQuery('.menu ul').toggleClass('active');

        e.preventDefault();
    });
    // Funcao dispara quando um link de materia é clicado
    $(".link_materia").click(function () {

        gtag('event', 'clicou_em_link_materia')
    })
    $(".btn_menu").click(function () {

        gtag('event', 'btn_menu')
    })
    $(".comment").click(function () {
        $('.modal-comments').modal('show');
        gtag('event', 'click_comentario')

    })

    $(".btn-share").click(function () {
        gtag('event', 'compartilhamentos')

    })



});
