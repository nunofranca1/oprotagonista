/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/views/front/assets/js/customizacao.js":
/*!*********************************************************!*\
  !*** ./resources/views/front/assets/js/customizacao.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  $("#formulario").submit(function (event) {
    event.preventDefault();
    var rota = '/contato';
    $.ajax({
      type: "POST",
      //dataType: "json",
      url: rota,
      data: $("#formulario").serialize(),
      beforeSend: function beforeSend() {
        var timerInterval;
        Swal.fire({
          title: 'Sua mensagem está sendo enviada!',
          html: 'Tempo previsto <b></b> milisecundos.',
          timer: 2000,
          timerProgressBar: true,
          didOpen: function didOpen() {
            Swal.showLoading();
            timerInterval = setInterval(function () {
              var content = Swal.getContent();

              if (content) {
                var b = content.querySelector('b');

                if (b) {
                  b.textContent = Swal.getTimerLeft();
                }
              }
            }, 100);
          },
          willClose: function willClose() {
            clearInterval(timerInterval);
          }
        }).then(function (result) {
          /* Read more about handling dismissals below */
          if (result.dismiss === Swal.DismissReason.timer) {
            console.log('I was closed by the timer');
          }
        });
      },
      success: function success(data) {
        if (data = 1) {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Mensagem enviada com sucesso',
            showConfirmButton: false,
            timer: 1500
          });
          $("input[type=text]").val('');
          $("input[type=email]").val('');
          $("textarea").val('');
        } else {
          alert('Mensagem não enviado');
        }
      }
    });
  });
  var slide_wrp = ".side-menu-wrapper"; //Menu Wrapper

  var open_button = ".menu-open"; //Menu Open Button

  var close_button = ".menu-close"; //Menu Close Button

  var overlay = ".menu-overlay"; //Overlay
  //Initial menu position

  $(slide_wrp).hide().css({
    "right": -$(slide_wrp).outerWidth() + 'px'
  }).delay(50).queue(function () {
    $(slide_wrp).show();
  });
  $(open_button).click(function (e) {
    //On menu open button click
    e.preventDefault();
    $(slide_wrp).css({
      "right": "0px"
    }); //move menu right position to 0

    setTimeout(function () {
      $(slide_wrp).addClass('active'); //add active class
    }, 50);
    $(overlay).css({
      "opacity": "1",
      "width": "100%"
    });
  });
  $(close_button).click(function (e) {
    //on menu close button click
    e.preventDefault();
    $(slide_wrp).css({
      "right": -$(slide_wrp).outerWidth() + 'px'
    }); //hide menu by setting right position

    setTimeout(function () {
      $(slide_wrp).removeClass('active'); // remove active class
    }, 50);
    $(overlay).css({
      "opacity": "0",
      "width": "0"
    });
  });
  $(document).on('click', function (e) {
    //Hide menu when clicked outside menu area
    if (!e.target.closest(slide_wrp) && $(slide_wrp).hasClass("active")) {
      // check menu condition
      $(slide_wrp).css({
        "right": -$(slide_wrp).outerWidth() + 'px'
      }).removeClass('active');
      $(overlay).css({
        "opacity": "0",
        "width": "0"
      });
    }
  });
});

/***/ }),

/***/ "./resources/views/front/assets/js/main.js":
/*!*************************************************!*\
  !*** ./resources/views/front/assets/js/main.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($) {
  "use strict";
  /*==================================================================
  [ Validate ]*/

  var input = $('.validate-input .input100');
  $('.validate-form').on('submit', function () {
    var check = true;

    for (var i = 0; i < input.length; i++) {
      if (validate(input[i]) == false) {
        showValidate(input[i]);
        check = false;
      }
    }

    return check;
  });
  $('.validate-form .input100').each(function () {
    $(this).focus(function () {
      hideValidate(this);
    });
  });

  function validate(input) {
    if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
      if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
        return false;
      }
    } else {
      if ($(input).val().trim() == '') {
        return false;
      }
    }
  }

  function showValidate(input) {
    var thisAlert = $(input).parent();
    $(thisAlert).addClass('alert-validate');
  }

  function hideValidate(input) {
    var thisAlert = $(input).parent();
    $(thisAlert).removeClass('alert-validate');
  }
})(jQuery);

/***/ }),

/***/ 1:
/*!*********************************************************************************************************!*\
  !*** multi ./resources/views/front/assets/js/customizacao.js ./resources/views/front/assets/js/main.js ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\Users\Nuno\Documents\projetos\oprotagonista\resources\views\front\assets\js\customizacao.js */"./resources/views/front/assets/js/customizacao.js");
module.exports = __webpack_require__(/*! C:\Users\Nuno\Documents\projetos\oprotagonista\resources\views\front\assets\js\main.js */"./resources/views/front/assets/js/main.js");


/***/ })

/******/ });
