@component('mail::message')
Mensagem de {{ $sender->contact->name }}

Assunto: {{ $sender->subject }}

**{{ $sender->mensagem }}**

*Email: {{ $sender->contact->email }}

*Telefone: {{ $sender->contact->phone }}

@endcomponent
