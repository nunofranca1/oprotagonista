@csrf
<div class="row">

    <div class="col-12 col-lg-7 col-md-7">
        <textarea class="form-control" aria-label="With textarea" rows="10" name="content"></textarea>
    </div>

    <div class="col-12 col-lg-5 col-md-5">
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12">

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text " id="basic-addon3"><i class="text-info fab fa-facebook-square"></i></span>
                    </div>
                    <input type="text" class="form-control" id="facebook" aria-describedby="basic-addon3" name="facebook" placeholder="Endereço Facebook">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12">

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text " id="basic-addon3"><i class="text-info fab fa-twitter-square"></i></span>
                    </div>
                    <input name="twitter" placeholder="Endereço Twitter" type="text" class="form-control" id="twitter" aria-describedby="basic-addon3">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12">

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text " id="basic-addon3"><i class="text-danger fab fa-instagram"></i></span>
                    </div>
                    <input type="text" name="instagram" placeholder="Endereco Instagram" class="form-control" id="instagram" aria-describedby="basic-addon3">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12">

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text " id="basic-addon3"><i class="text-danger fab fa-youtube"></i></span>
                    </div>
                    <input type="text" name="youtube" placeholder="Endereço Youtube" class="form-control" id="youtube" aria-describedby="basic-addon3">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12">

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text " id="basic-addon3"><i class="text-success fab fa-whatsapp-square"></i></span>
                    </div>
                    <input type="text" name="whatstapp" placeholder="Número do Whatsapp" class="form-control" id="whatsapp" aria-describedby="basic-addon3">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-12 col-md-12">
                <button type="submit" class="btn btn-lg btn-block btn-success mt-3">ATUALIZAR</button>

            </div>
        </div>

    </div>
</div>
