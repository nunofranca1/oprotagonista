@extends('layouts.admin')
@section('content')
    <form action="{{ route('configs.store') }}" method="POST" enctype= "multipart/form-data">

    @include('page.configs._partials.form')
    </form>
    <script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>
    <script src="{{ asset('assets/js/ckEditor.js') }}"></script>
@endsection
