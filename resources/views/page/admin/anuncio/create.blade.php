@extends('layouts.admin')
@section('content')
<section>
	<div class="row">
		<div class="col-12 col-lg-8 col-md-8">
			<div class="alert alert-warning">
				ATENÇÃO: Por padrão os anúncios são cadastratos com status desativado. Para ativar navegue até a aba configurações.
			</div>

			<form enctype="multipart/form-data" action="{{ route('anuncio.novo.store') }}" method="post">
				@csrf
			  <div class="form-group">
			    <label for="nome_anuncio1">Nome do anúncio</label>
			    <input value="{{ old('nome') }}" type="text" class="form-control @error('nome') is-invalid @enderror" id="nome_anuncio" name="nome" aria-describedby="emailHelp" placeholder="Informe uma identificação">
			  @error('nome')
				  <div class="invalid-feedback">
					  <strong>{{$message}}</strong>
				  </div>
				  @enderror
			  </div>
			  <div class="form-group">
			  	<div class="row">
			  		<div class="col-12 col-md-6 col-lg-6">
			  			<label for="exampleInputEmail1">Tamanho do anúncio</label>
			  			<select  class="form-control @error('tamanho') is-invalid @enderror" id="tamanho_anuncio" name="tamanho">
				    		<option value="">Formato do anúncio</option>
				    		<option value="468x60">468x60px</option>
				    		<option value="333x333">333x333px</option>

			    		</select>
						@error('tamanho')
						<div class="invalid-feedback">
							<strong>{{$message}}</strong>
						</div>
						@enderror
			  		</div>
			  		<div class="col-12 col-md-6 col-lg-6">
			  			<label for="img_anuncio">Imagem do anúncio</label>
			  			<input type="file" name="img" id="img_anuncio" class="form-control @error('img') is-invalid @enderror">
						@error('img')
						<div class="invalid-feedback">
							<strong>{{$message}}</strong>
						</div>
						@enderror
					</div>
			  	</div>
			  </div>
			  <div class="form-group">
			   	<div class="row">
			   		<div class="col-12 col-md-12 col-lg-12">
			   			 <label for="exampleInputEmail1">Link</label>
			   			 <input type="text" class="form-control" id="link_anuncio" aria-describedby="emailHelp" placeholder="EX: https://www.exemplo.com.br" name="link">
			   		</div>

			   	</div>
			  </div>

			  <button type="submit" class="btn btn-primary">CADASTRAR</button>
			</form>
		</div>
	</div>
</section>
@endsection
