@extends('layouts.admin')
@section('content')

<table class="table" >
  <thead>
    <tr class=" text-center">
      <th scope="col">Anunciante</th>
      <th>Tamanho</th>
      <th width="400px">Anuncio</th>
      <th scope="col">Visibilidade</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($anuncios as $anuncio)
    <tr>
      <td scope="row">{{ $anuncio->nome }}</td>
      <td>{{ $anuncio->tamanho }}</td>

      <td class="text-center">
          <button class="btn btn-info">VER ANUNCIO</button>

{{--      	<img src='{{ url("storage/{$anuncio->img}") }}' width="100%">--}}

      </td>
      <td class="text-center">
      	@if($anuncio->status == 1)


        <form action="{{ route('anuncio.update',[$anuncio->id]) }}" method="post">
          @csrf
          @method('PUT')
          <button type="submit" class="btn btn-success">ATIVADO</button>

        </form>

      	@else
        <form action="{{ route('anuncio.update',[$anuncio->id]) }}" method="post">
          @csrf
          @method('PUT')
          <button type="submit" class="btn btn-danger">DESATIVADO</button>

        </form>


      	@endif
      </td>
      <td>
      	<form action="{{ route('anuncio.delete',[$anuncio->id]) }}" method="post" >
      		@csrf
      		@method('delete')

      	 <button type="submit" class="btn btn-danger btn-block"><i class="fa fa-2x fa-trash" aria-hidden="true"></i></button>
		</form>
      </td>
    </tr>
    @endforeach

  </tbody>
</table>

@endsection
