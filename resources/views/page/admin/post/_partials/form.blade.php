@csrf
<div class="form-group">
    <label for="title">Título</label>

    <input value="{{ $post->title ?? old('title') }}" type="text"
           class="form-control @error('title') is-invalid @enderror" id="title" placeholder="Título da matéria"
           name="title">
    @error('title')
    <div class="invalid-feedback">
        {{$message}}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="subtitle">Subtítulo</label>
    <input value="{{ $post->subtitle ?? old('subtitle') }}" type="text"
           class="form-control @error('subtitle') is-invalid @enderror" id="subtitle" placeholder="Subtítulo da matéria"
           name="subtitle">
    @error('title')
    <div class="invalid-feedback">
        {{$message}}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="materia">Matéria</label>
    <textarea class="form-control @error('content') is-invalid @enderror" id="materia" rows="3" name="content">
            {{ $post->content ?? old('content') }}
    </textarea>
    @error('content')
    <div class="invalid-feedback">
        {{$message}}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="imagem">Foto</label>
    <input type="file" class="form-control @error('imagem') is-invalid @enderror" name="imagem[]" id="imagem"
           multiple>
    @error('imagem')
    <div class="invalid-feedback">
        {{$message}}
    </div>
    @enderror
</div>

<div class="form-group">
    <div class="row">
        <div class="col-6">
            <label>Vídeo</label>
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fab fa-youtube"></i></div>
                </div>
                <input value="{{ $post->movies[0]->link ?? old('post') }}" type="text" name="link_movie" class="form-control"
                       id="movie" placeholder="Link do Video">
            </div>
        </div>
        <div class="col-6">
            <label for="editoria">Tags</label>
            <input type="text" class="form-control @error('tag') is-invalid @enderror" id="editoria"
                   placeholder="Separedo por vírgula" name="tag"
                   value="{{ $post->tag ?? old('tag') }}">
            @error('tag')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>

</div>


<div class="form-group">
    <div class="row">
        <div class="col-6">
            <label for="categoria">Editoria</label>
            <select class="form-control @error('cateogry_id') is-invalid @enderror" name="category_id" id="categoria">
                <option value="">Informa a editoria</option>
                @foreach($categorias as $categoria)
                    <option value="{{ $categoria['id'] }}">{{ $categoria['name'] }}</option>
                @endforeach
            </select>
            @error('category_id')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
        <div class="col-6">
            <div class="row">
                <div class="col-8">
                    <label for="agendar">Data Publicação</label>
                </div>
                <div class="col-4" align="right">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="checado" id="agendar" name="chekbox">
                        <label class="form-check-label" for="defaultCheck1">
                            AGENDAR
                        </label>
                    </div>
                </div>
            </div>
            <input type="datetime-local" class="form-control" placeholder="Subtítulo da matéria" name="date_hour"
                   id="data_hora">

        </div>
    </div>


</div>
<div class="form-group">




</div>

