@extends('layouts.admin')
@section('content')
    <div class="col-12">
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12">
                <div class="form-group">
                    <form action="{{ route('postagem.update', [$post->id]) }}" method="POST"
                          enctype="multipart/form-data">

                        @method('PUT')
                        @include('page.admin.post._partials.form')

                        <button type="submit" class="btn btn-lg btn-block btn-success mb-3">ATUALIZAR</button>

                    </form>
                </div>
                <div class="form-group">
                    <form action="{{route('postagem.delete', [$post->id])}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-lg btn-block btn-danger mb-3">DELETAR</button>

                    </form>
                </div>

            </div>

        </div>
    </div>

@endsection
