 @extends('layouts.admin')
@section('content')

<div class="row">
  <div class="col-12 col-lg-12 col-md-12">
    <form action="{{ route('postagem.store') }}" method="POST" enctype= "multipart/form-data">
      @include('page.admin.post._partials.form')
        <button type="submit" id="cadastrar" class="btn btn-lg btn-block btn-success mb-3">CADASTRAR</button>
    </form>
  </div>
</div>

@endsection
