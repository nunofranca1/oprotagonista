@extends('layouts.admin')
@section('content')
    <x-analytics-dados />
		<table class="table table-striped">
  <thead>
    <tr>

      <th scope="col-4">Titulo</th>

       <th scope="col-2">Comentários</th>
      <th scope="col-2">Ações</th>
    </tr>
  </thead>
  <tbody>
    @foreach($posts as $post)
    <tr>


      <td class="col-4">
        <small>{{ \Carbon\Carbon::parse($post->date_hour)->format('d/m/y H:i')}}</small>
       <p> {{ $post->title }}</p>
      </td>
      @foreach($dadosPost as $numeros)
        @if($numeros["pageTitle"] === $post['title'])
      <td>
        {{$numeros["pageViews"]}}
      </td>
          @endif
      @endforeach
    <x-analytics-dados/>
    <table class="table table-striped">
        <thead>
        <tr>

            <th scope="col-4">Titulo</th>
            <th scope="col-2">Visitas</th>
            <th scope="col-2">Comentários</th>
            <th scope="col-2">Ações</th>
        </tr>
        </thead>
        <tbody>

        @foreach($posts as $post)
            <tr>


                <td class="col-4">
                    <small>{{ \Carbon\Carbon::parse($post->date_hour)->format('d/m/y H:i')}}</small>
                    <p>{{ $post->title }}</p>
                </td>

                <td>
                    @foreach($dadosPost as $numeros)

                        @if($numeros["url"] === "/noticia/".$post->link)

                            {{$numeros["pageViews"]}}

                        @endif

                    @endforeach
                </td>


                <td class="col-2">{{count($post->comments)}}</td>
                <td class="col-2">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="{{ route('postagem.edit', $post->id) }}" type="button"
                           class="far text-primary fa-edit"></a>

                    </div>
                </td>
            </tr>
        @endforeach
        {{ $posts->links() }}

        </tbody>
    </table>
@endsection

