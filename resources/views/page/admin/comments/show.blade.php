@extends('layouts.admin')
@section('content')
    <section>
        <p><h4>Comentários da matéria:</h4> </p>
        <p><h6><a href="{{"/noticia/".$post->link}}" target="_blank"> {{ $post->title }}</a></h6></p>
    </section>
    <table class="table table-striped">
        <thead>
        <tr class="d-flex">
            <th scope="col" class="col-2">Autor</th>
            <th class="col-6">Comentário</th>
            <th class="col-2">Data/hora</th>
            <th class="col-2">Publicado</th>
        </tr>
        </thead>
        <tbody>
        @foreach($post->comments as $comment)
            <tr class="d-flex">
                <th scope="row" class="col-2">Anônimo</th>
                <td class="col-6">
                    <p>{{ $comment->comment }}</p>
                </td>
                <td class="col-2">{{ \Carbon\Carbon::parse($comment->created_at)->format('d-m-Y H:i')}}</td>

                <form action="{{ route('comment.update', ['id' => $comment->id]) }}" method="post">
                    @csrf
                    {{--                    @method('PUT')--}}
                    <td class="col-2">
                        @if($comment->status =="0")
                            <button class="btn btn-danger btn-block rounded-0">Não</button>
                        @else
                            <button class="btn btn-success btn-block rounded-0">Sim</button>
                        @endif
                    </td>
                </form>

            </tr>
        @endforeach

        </tbody>
    </table>
@endsection
