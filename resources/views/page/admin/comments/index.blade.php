@extends('layouts.admin')
@section('content')
<table class="table table-striped">
    <thead>
    <tr class="d-flex">
        <th scope="col" class="col-2">Autor</th>
        <th class="col-6">Comentário</th>
        <th class="col-2">Data/hora</th>
        <th class="col-2">Publicado</th>
    </tr>
    </thead>
    <tbody>
    @foreach($comments as $comment)
        <tr class="d-flex">
            <th scope="row" class="col-2">{{ isset($comment->contact->name) ? $comment->contact->name : "Anônimo" }}</th>
            <td class="col-6">
                <p>{{ $comment->comment }}</p>
                <small><b>MATÉRIA:</b><a href="{{'/noticia/'.$comment->post->link}}" target="_blank"> {{ $comment->post->title}}</a></small>
            </td>
            <td class="col-2">{{ \Carbon\Carbon::parse($comment->created_at)->format('d-m-Y H:i')}}</td>

                <form action="{{ route('comment.update', ['id' => $comment->id]) }}" method="post">
                    @csrf
{{--                    @method('PUT')--}}
                    <td class="col-2">
                        @if($comment->status =="0")
                            <button class="btn btn-danger btn-block rounded-0">Não</button>
                        @else
                            <button class="btn btn-success btn-block rounded-0">Sim</button>
                        @endif
                    </td>
                </form>

        </tr>
    @endforeach

    </tbody>
</table>
    @endsection
