@extends('layouts.admin')
@section('content')
    <section class="container">
        <div class="row">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="col-12">
                    <button type="button" class=" rounded-0 btn btn-success" data-toggle="modal"
                            data-target="#exampleModalLong">
                        Adicionar vídeo
                    </button>
                </div>
            </div>

            <div class="col-12 col-lg-12 col-md-12">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Título</th>
                        <th scope="col">Status</th>
                        <th scope="col">Deletar</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($movies as $movie)
                        <tr>
                            <td scope="row">
                                <small>{{ \Carbon\Carbon::parse($movie->created_at)->format('d/m/y H:i')}}</small>
                                <p> {{ $movie->title ?? $movie->post->title}}</p>
                            </td>
                            <td>

                                @if($movie->tv==1)
                                    <form method="post" action="{{ route('tv.update', $movie->id) }}">
                                        @method('put')
                                        @csrf
                                        <button type="submit" class="btn btn-success rounded-0">Ativado</button>
                                    </form>
                                @else
                                    <form method="post" action="{{ route('tv.update', $movie->id) }}">
                                        @method('put')
                                        @csrf
                                        <button type="submit" class="btn btn-info rounded-0">Desativado</button>
                                    </form>
                                @endif

                            </td>
                            <td>
                                <form method="post" action="{{ route('tv.delete', $movie->id) }}">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger rounded-0">Deletar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Cadastro de vídeo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('tv.store')}}">
                        @csrf
                        <div class="form-group">
                            <label for="titulo">Título</label>
                            <input type="text" name="title" class="form-control" id="titlo"
                                   placeholder="Título do vídeo">
                        </div>
                        <div class="form-group">
                            <label for="movie">Informe o link do vídeo</label>
                            <input type="text" name="link" class="form-control" id="movie" placeholder="EX: https://youtu.be/wlcUYWnh7MY">
                        </div>
                        <button class="btn btn-success btn-block">
                            Cadastrar vídeo
                        </button>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
