@extends('layouts.admin')
@section('content')
    <section class="container">
        <div class="row">
            <div class="col-12 col-lg-7 col-md-7">
                <form method="post" action="{{ route('categoria.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Novo Editorial</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="name" aria-describedby="emailHelp" placeholder="Nome da Categoria">

                    </div>

                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </form>
            </div>
            <div class="col-12 col-lg-5 col-md-5 bg-white rounded-lg">
                <div class="alert alert-dark m-0 rounded-0 container-fluid text-center">
                    Editorias cadastradas
                </div>
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Artigos</th>
                        <th scope="col">Desativar</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($editorias as $editoria)
                    <tr>
                        <td scope="row">{{ $editoria->id  }}</td>
                        <td>{{ $editoria->name  }}</td>
                        <td>{{ count($editoria->posts) }}</td>
                        <td><i class="fa fa-trash text-ligth" aria-hidden="true"></i></td>

                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection
