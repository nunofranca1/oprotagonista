@extends('page.site.master.master')
@section('content')
    <x-anuncios-topo />
    <section class="row">

        <div class="col-12 col-md-9 col-xl-9 col-lg-9">
            @include('page.site._patials.shownoticia')
            <x-comments :post="$postagem->id" />
            <x-leia-mais />

        </div>
        <div class="col-12 col-md-3 col-xl-3 col-lg-3">
          <x-anuncios-laterais />
        </div>
    </section>

    <section id="newlestter" class="container-fluid">

    </section>
    <section id="footer">
        @include('page.site._patials.footer')
    </section>
@endsection
