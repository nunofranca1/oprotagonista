@extends('page.site.master.master')
@section('content')
    <section class="row">
        <x-anuncios-topo />
        <div class="col-12 col-md-9 col-xl-9 col-lg-9">

            <h2 class="mt-0 mb-3">Resultados para {{ $search }}</h2>

                <ul>
                    @foreach($noticias as $noticia)
                        <li>
                            <div class="info-noticia text-white text-secondary atulizacao">
                                Atualizado em
                                {{ \Carbon\Carbon::parse($noticia->updated_at)->format('d-m-Y  H:i')}}
                            </div>
                            <a href="{{ route('noticia.show', $noticia->link) }}" class="link_materia">
                                <h1 class="titulo-principal">{{ $noticia->title }}</h1>
                            </a>
                            <h5 class="subtitle">{{$noticia->subtitle}}</h5>
                        </li>
                        <hr>
                    @endforeach
                </ul>


        </div>
        <div class="col-12 col-md-3 col-xl-3 col-lg-3">
            <x-anuncios-laterais />
        </div>
    </section>

    <section id="newlestter" class="container-fluid">

    </section>
    <section id="footer">
        @include('page.site._patials.footer')
    </section>
@endsection
