@extends('page.site.master.master')

@section('content')
    <x-anuncios-topo />
    <section id="destaques">

        <div class="row">

            <div class="col-md-7">
                @include('page.site._patials.manchete')

            </div>
            <div class="col-md-5">
                @include('page.site._patials.noticias_secundarias')

               <x-tv-oprotagonista />

            </div>
            <div class="col-12">
                @include('page.site._patials.noticias_terciarias')
            </div>
        </div>

    </section>
    <section id="mais-noticias-anuncios">
        <div class="row">
            <div class="col-12 col-lg-9 col-md-9 col-xl-9">

                <x-mais-noticias />
                <x-leia-mais />

            </div>
            <div class="col-12 col-md-3 col-lg-3">
                <x-anuncios-laterais />
            </div>
        </div>
    </section>

    <section id="footer">
        @include('page.site._patials.footer')
    </section>
@endsection
