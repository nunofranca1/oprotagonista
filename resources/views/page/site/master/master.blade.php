<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-184795553-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-184795553-2');
    </script>


    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @isset($postagem->title)
        <title>{{ $postagem->title }}</title>
        <meta name="Description" content="{{ $postagem->title  }}">
        <meta name="Subject" content="{{ $postagem->subtitle  }}">
        <meta name="Title" content="{{ $postagem->title }}">
        <meta property="og:locale" content="pt_BR"/>
        <meta property="og:type" content="article"/>
        <meta property="og:url" content="https://www.oprotagonistafsa.com.br/noticia/{{ $postagem->link }}"/>
        <meta property="og:title" content="{{ $postagem->title }}"/>
        <meta property="og:site_name" content="O Protagonista"/>
        <meta property="og:description" content="{{ $postagem->subtittle }}"/>
        <meta property="og:image"
              content="https://www.oprotagonistafsa.com.br/storage/{{$postagem->imgs[0]->miniatura}}"/>
        <meta property="og:image:type" content="image/jpeg">
        <meta property="og:image:type" content="image/jpg">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:alt" content="{{ $postagem->title }}"/>
        <meta property="og:image:width" content="652"/>
        <meta property="og:image:height" content="408"/>
    @else
        <title> O Protagonista FSA | Notícias atualizadas de Feira de Santana e região</title>
        <meta name="Title" content="O Protagonista FSA | Notícias atualizadas de Feira de Santana e região">
        <meta name="Subject" content="Notícias atualizada da cidade de Feira de Santana">
        <meta name="description" content="Site de Notícias de Feira de Santana e região">
    @endif
    <meta charset="UTF-8">
    <meta name="author" content="Augusto Ferreira - O Protagonista FSA">
    <meta
        name="O Protagonista FSA - Augusto Ferreira | Site de notícias de Feira de santana - Bahia – https://www.oprotagonistafsa.com.br"
        content="feira de santana, feira, feira santana, micareta, micareta de feira de santana, prefeitura de feira, feiradesantana, noticias feira de santana, jornal, Colbert, Prefeito de Feira, noticias de feira, politica feira, politica de feira de santana">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="index, no follow">
    <meta name="googlebot" content="all">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('assets/site/css/load_fonts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/customizacao-geral.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/site/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"/>
    <link rel="shortcut icon" href="{{asset('asset/site/img/logo.png')}}" type="image/x-icon" />
    @toastr_css
</head>
<body id="index">
<!-- Global site tag (gtag.js) - Google Analytics -->

{{--script da API--}}
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-189111319-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-189111319-1');
</script>

<div class="row">


    <div class="col-md-4 col-lg-4 col-xl-4 col-2" width="100%">

    </div>
    <div class="col-md-4 col-lg-4 col-xl-4 col-8">
        @include('page.site.master.includes.logo')
    </div>
    <div class="col-md-4 col-lg-4 col-xl-4 col-2">

    </div>
</div>
@include('page.site.master.includes.menu')



<section class="container">
    @yield('content')
</section>

@include('page.site.master.includes.sobre')
@include('page.site.master.includes.contact')


<script src="{{ asset('assets/site/js/app.js')}}"></script>
<script src="{{ asset('assets/site/js/libs/jquery.js')}}"></script>

<script src="{{ asset('assets/site/js/script-geral-custom.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script src="{{ asset('assets/site/js/customizacao.js') }}"></script>
<script src="{{ asset('assets/site/js/style.js') }}"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-KN80G51LH4"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'G-KN80G51LH4');

</script>
<script src="https://unpkg.com/react@17/umd/react.development.js" crossorigin></script>
<script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js" crossorigin></script>

</body>
@jquery
@toastr_js
@toastr_render
</html>
