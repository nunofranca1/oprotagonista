<nav>
    <div class="menu-bottom d-none d-md-block d-lg-block d-xl-block">

        <div class="container">
            <form id="busca" class="form mt-1" method="GET" action="{{ route('noticias.search') }}">
                <div class="form-group">
                    <input type="text" class="form-group" name="q" placeholder="O que procura?"/>
                </div>
                <div class="form-group">
                    <button type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </form>
            <div class="menu-itens">
                <a href="{{ route('home.index')  }}">
                    HOME
                </a>
                <a href="#" data-toggle="modal" data-target="#exampleModal">
                    SOBRE
                </a>
                <a href="#" data-toggle="modal" data-target="#modalContact">
                    FALE CONOSCO
                </a>
            </div>
        </div>
    </div>
</nav>


<!-- menu mobile -->
<section class="d-block d-sm-block d-md-none hide">
    <div class="bg-success text-right">
        <div class="container">
            <div class="row">

                <div class="col-9">
                    <form id="busca" class="form" method="GET" action="{{ route('noticias.search') }}">
                        <div class="form-group">
                            <input type="text" class="form-group" name="q" placeholder="O que procura?"/>
                        </div>
                        <div class="form-group">
                            <button type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </form>
                </div>

            </div>


        </div>
    </div>
    <div class="col-12 mt-2 fixed-bottom bg-white shadow-lg">
        <div class="row p-2">
            <div class="col-4 text-center">
                <a href="#" data-toggle="modal" data-target="#modalContact">
                <i class=" fa fa-2x text-danger fa-envelope" aria-hidden="true"></i>
                </a>
            </div>
            <div class="col-4 text-center">
                <a href="#" data-toggle="modal" data-target="#exampleModal">
                    <i class=" fa fa-2x text-danger fa fa-users" aria-hidden="true"></i>
                </a>
            </div>
            <div class="col-4 text-center">
                    <a class="btn-share" @isset($postagem->link) href="https://api.whatsapp.com/send?text=https://www.oprotagonistafsa.com.br/noticia/{{$postagem->link}}" @else href="https://api.whatsapp.com/send?text=https://www.oprotagonistafsa.com.br/" @endif
                       target="_blank">
                    <i class="fa fa-share fa-2x text-danger" aria-hidden="true"></i>
                    </a>

            </div>
        </div>

    </div>

</section>

