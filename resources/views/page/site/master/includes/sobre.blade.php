<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sobre <strong>O PROTAGONISTA</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-justify">
                <p>
                    O Protagonista surgiu com a intenção de ser um diferencial entre sites e blogs em Feira de Santana e
                    região. Um instrumento para levar aos internautas notícias com qualidade de texto e informações bem
                    apuradas, principalmente no cenário político.
                    Passados 1 ano e meio, O Protagonista, graças a Deus e a seus leitores, conseguiu se consolidar. É
                    uma referência entre blog políticos, fruto de muito trabalho.
                    Com este novo layout e tecnologia repaginamos o visual e manuseio, porém o conteúdo e objetivos
                    seguem os mesmos: levar informação “mastigada” aos nossos internautas, sempre primando pelo bom
                    jornalismo.
                </p>
                <br/>
                <p>
                    <strong>SOBRE O EDITOR</strong> – Jornalista há 31 anos, Augusto Ferreira é um dos “filhos” do
                    extinto jornal Feira Hoje. Nessa longa jornada profissional, passagens pelos maiores órgãos de
                    comunicação de Feira e do estado, dentre eles: TV Subaé, Feira Hoje, Tribuna Feirense, Folha do
                    Estado, Jornal do Brasil. Passagens pelas assessorias da Câmara de Vereadores de Feira, Prefeitura
                    de Feira, Assembleia Legislativa da Bahia, além de chefiar a assessoria de comunicação em diversas
                    prefeitura baianas.
                    No Protagonista, temos o compromisso de sermos imparciais. E que Deus permita levarmos adiante esta
                    tarefa.
                </p>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <img class="logo" width="100%" src="{{ asset('assets/site/img/logo.png') }}" alt="O Protagonista">

                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
