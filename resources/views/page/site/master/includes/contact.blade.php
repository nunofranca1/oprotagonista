<!-- Modal -->
<div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <img width="100%" src="{{asset('assets/site/img/logo.png')}}">
                </div>


                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-justify">

                <form id="contact" action="{{route('contato.store')}}" method="post">
                    @csrf

                    <div class="form-group">
                        <label for="name">Informe seu nome</label>
                        <input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp"
                               placeholder="Informe seu nome">

                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" name="email" id="email"
                               placeholder="Informe seu e-mail">
                    </div>
                    <div class="form-group">
                        <label for="phone">Telefone</label>
                        <input type="text" name="phone" class="form-control" id="telefone"
                               placeholder="Informe seu telefone">
                    </div>
                    <div class="form-group">
                        <label for="subject">Assunto</label>
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Qual assunto?">
                    </div>
                    <div class="form-group">
                        <label for="message">Mensagem</label>
                        <textarea rows="4" class="form-control" name="mensagem" id="message"></textarea>
                    </div>

                    <div class="form-group">
                        <div class="row">

                            <div class="col-12 col-sm-8 col-lg-8 col-md-8">

                            </div>
                            <div class="col-12 col-sm-4 col-lg-4 col-md-4">
                                <button type="submit"
                                        class="btn border-success text-dark btn-outline-success btn-block">ENVIAR
                                </button>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
