<h2 class="titulo">Notícias <span>em destaque</span></h2>
<div class="noticia-principal">
    <a href="{{ route('noticia.show', $posts['0']->link) }}" class="link_materia">
        <h1 class="titulo-principal">{{$posts['0']->title}}</h1>
        <div class="imagens" >
            <img class="d-none d-sm-block rounded-sm d-md-block d-lg-block d-xl-block"
                 src='{{ url("storage/{$posts[0]->imgs[0]->desktop}") }}'>
            <img class="rounded-sm d-block d-sm-none" src='{{ url("storage/{$posts[0]->imgs[0]->mobile}") }}'>
        </div>
    </a>
    <div class="bg-danger">
        <p>{{ $posts['0']->subtitle }} <a href="{{ route('noticia.show', $posts['0']->link) }}">Ler mais..</a></p>
    </div>
</div>




