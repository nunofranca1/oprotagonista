
    <div class="noticias-destaques mt-3">
        <div class="noticia-destaque">
            <div class="row">
                @foreach($posts as $post)
                    @if($loop->index >= 1 and $loop->index < 3)

                <div class="col-12 col-lg-6 col-md-12 col-xl-6">

                        <span class="text-dark ">{{$post->category->name}}  - O Protagonista</span>

                    <a href="{{ route('noticia.show', $post->link) }}" class="link_materia">
                        <div class="noticia-destaque-texto">
                            <div class="info-noticia text-white text-secondary atulizacao">
                                Atualizado em
                                {{ \Carbon\Carbon::parse($post->updated_at)->format('d-m-Y  H:i')}}
                            </div>
                            <h4 class="titulo-destaque mt-1">{{$post->title}}</h4>
                            <div class="imagens mt-1" >
                                <img width="100%" class="d-none d-sm-block rounded-sm d-md-block d-lg-block d-xl-block"
                                     src='{{ url("storage/{$post->imgs[0]->desktop}") }}'>
                                <img width="100%" class="rounded-sm d-block d-sm-none" src='{{ url("storage/{$post->imgs[0]->mobile}") }}'>
                            </div>

                        </div>
                    </a>

                    <hr class="d-block d-sm-none">

                </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>


