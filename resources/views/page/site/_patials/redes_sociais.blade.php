<div class="row text-right">
    <div class="d-sm-none d-md-block text-right p-3 ">
        <i class="fab fa-instagram fa-2x text-danger"></i>
        <i class="fab fa-facebook-square fa-2x text-info"></i>
        <i class="fab fa-youtube fa-2x text-danger"></i>
    </div>
</div>
