
    <div class="noticias-destaques mt-3">
        <div class="noticia-destaque">
            <div class="row">
                @for($i=3 ; $i<7; $i++)
                <div class="col-12 col-lg-3 col-md-3 col-xl-3 bg-light">

                        <span class="text-dark ">{{$posts[$i]->category->name}}  - O Protagonista</span>

                    <a href="{{ route('noticia.show', $posts[$i]->link) }}" class="link_materia">
                        <div class="noticia-destaque-texto">
                            <div class="info-noticia text-white text-secondary atulizacao">
                                Atualizado em
                                {{ \Carbon\Carbon::parse($posts[$i]->updated_at)->format('d-m-Y  H:i')}}
                            </div>
                            <h4 class="titulo-destaque mt-1">{{$posts[$i]->title}}</h4>
                            <p>{{ $posts[$i]->subtitle }} </p>
                        </div>
                    </a>
                    <hr class="d-block d-sm-none">
                </div>

                @endfor
            </div>
        </div>
    </div>


