
<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-10 col-sm-11 col-md-11">
                <h2 class="titulo">Notícia</h2>
            </div>
            <div class="col-sm-1 col-lg-1 col-2 text-right ">
                <a class="btn-share" href="https://api.whatsapp.com/send?text=https://www.oprotagonistafsa.com.br/noticia/{{ $postagem->link }}"
                   target="_blank">
                    <img src="{{asset('assets/site/img/whatsapp.png')}}" width="100%">
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <h1 class="titulo-principal">{{$postagem->title}}</h1>
        <h5 class="subtitle">{{$postagem->subtitle}}</h5>
        <div class="mt-3">
            @isset($postagem->imgs[0])
            <div class="">
                <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block noticia-imagem ">

                    <img  class="img rounded-sm" width="100%" src="{{ url("storage/{$postagem->imgs[0]->desktop}") }}" alt="{{$postagem->title}}">
                </span>
                <span class="d-block d-sm-none ">
                    <img class="img rounded-sm" width="100%" src="{{ url("storage/{$postagem->imgs[0]->mobile}") }}" alt="{{$postagem->title}}">
                </span>
            </div>
            @endif
            <div class="video container mb-3 mt-3">
                @foreach($postagem->movies  as $movie)
                    <div class="row">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{$movie->link}}"
                                    allowfullscreen></iframe>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="texto-materia">
                {!! $postagem->content  !!}
            </div>
        </div>
        <div>
            <form method="post" action="{{ route('post.comments', $postagem->link) }}">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Deixe seu comentário</label>
                    <textarea name="comment" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-outline-success btn-block">
                        ENVIAR COMENTÁRIO
                    </button>
                </div>
            </form>

        </div>
    </div>

</div>
