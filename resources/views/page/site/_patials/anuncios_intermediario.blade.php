
<div class="container">
    <div class="row">
        @foreach($anuncios_topo as $anuncio_topo)
            @if($anuncio_topo->tamanho == "468x60")
                <div class="col-12 publicidade-imagem">
                    <a href="{{ $anuncio_topo->link }}">
                        <img src='{{url("storage/{$anuncio_topo->img}")}}'>
                    </a>
                </div>
            @endif

        @endforeach

    </div>
</div>
