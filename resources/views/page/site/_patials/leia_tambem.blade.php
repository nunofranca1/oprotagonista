<div class="container">
    <h2 class="titulo">Leia <span>também</span></h2>
    <ul class="leia-tambem-lista">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-12">
                <li>
                    <a href="{{ route('noticia.show', $posts[14]->link) }}">{{$posts[14]->title}}</a>
                </li>
            </div>
            <hr>
            <div class="col-lg-6 col-md-6 col-12">
                <li>
                    <a href="{{ route('noticia.show', $posts[15]->link) }}">{{$posts[15]->title}}</a>
                </li>
            </div>
        </div>
    </ul>
</div>
