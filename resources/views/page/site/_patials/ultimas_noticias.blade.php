<h2 class="titulo">Mais <span>notícias</span></h2>
<div class="d-none d-sm-none d-md-block">
    <div class="row">

        @for($i=7; $i<=9; $i++)

                <div class="col-12 col-lg-4 col-md-4 col-xl-4">
                    <span class="text-dark ">{{$posts[$i]->category->name}}  - O Protagonista</span>
                    <div class="info-noticia text-white text-secondary atulizacao">
                        Atualizado em
                        {{ \Carbon\Carbon::parse($posts[$i]->updated_at)->format('d-m-Y  H:i')}}
                    </div>
                    <a href="{{ route('noticia.show', $posts[$i]->link) }}" class="link_materia">
                        <h2 class="title_h2 ">{{ $posts[$i]->title }}</h2>
                    </a>

                    <a href="{{ route('noticia.show', $posts[$i]->link) }}" class="link_materia">
                        @isset($posts[$i]->imgs[0])
                            <img width="100%" class="rounded-sm"
                                 src="{{ url('storage/'.$posts[$i]->imgs[0]->desktop) }}"
                                 alt="{{ $posts[$i]->title }}">
                        @endif
                    </a>
                </div>

        @endfor
    </div>

    <h2 class="titulo mt-3">Leia <span>também</span></h2>
    <div class="row">
        @for($i=10 ; $i<=12; $i++)

                <div class="col-12 col-lg-4 col-md-4 col-xl-4">
                    <a href="{{ route('noticia.show', $posts[$i]->link) }}" class="link_materia">
                        <span class="text-dark ">{{$posts[$i]->category->name}}  - O Protagonista</span>
                        <div class="info-noticia text-white text-secondary atulizacao">
                            Atualizado em
                            {{ \Carbon\Carbon::parse($posts[$i]->updated_at)->format('d-m-Y  H:i')}}
                        </div>
                        <h2 class="title_h2 ">{{ $posts[$i]->title }}</h2>

                    </a>
                </div>

        @endfor
    </div>
</div>



<div class="d-block d-md-none">
    @for($i=10 ; $i<=12; $i++)

            <a href="{{ route('noticia.show', $posts[$i]->link) }}" class="link_materia">
                <h2 class="title_h2">{{ $posts[$i]->title }}</h2>
            </a>
            <div class="info-noticia text-white text-secondary atulizacao">
                Atualizado em
                {{ \Carbon\Carbon::parse($posts[$i]->updated_at)->format('d-m-Y  H:i')}}
            </div>
            @isset($posts[$i]->imgs[0])

                    <a href="{{ route('noticia.show', $posts[$i]->link) }}">
                        <img width="100%" class="rounded-sm" src="{{ url('storage/'.$posts[$i]->imgs[0]->mobile) }}"
                             alt="{{ $posts[$i]->title }}">
                    </a>


            @endif
            <hr>

    @endfor

</div>







