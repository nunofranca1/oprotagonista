<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    $name = $this->faker->word;
    $url = \Illuminate\Support\Str::slug($name);
    return [
        'name' =>$name,
        'url' =>$url

    ];
});
