<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    $title = $this->faker->sentence('7');
    $link = Illuminate\Support\Str::slug($title);
    $img = $this->faker->image("storage/app/public/miniaturas/",1580,1580,'cats', null, false);
    return [
        'title' =>$title,
        'subtitle' => $this->faker->sentence('18'),
        'content'=>$this->faker->paragraph(5),
        'img'=> "miniaturas/{$img}",
        'date_hour'=>$this->faker->dateTime,
        'link' =>$link,
        'tag' =>'teste',
        'category_id' => function (){
        return factory(App\Category::class)->create()->id;
        }
    ];
});
