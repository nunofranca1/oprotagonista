<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Alberto',
            'email' => 'alberttojrfsa@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123mudar'),
            'remember_token' => Str::random(10),
        ]);
        \App\User::create([
            'name' => 'Augusto',
            'email' => 'gutto23@hotmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('august123#'),
            'remember_token' => Str::random(10),
        ]);
    }
}
