<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('senders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->longText('mensagem');
            $table->integer('status')->default('0');
            $table->unsignedInteger('contact_id');
            $table->foreign('contact_id')
                    ->references('id')
                    ->on('contacts')
                    ->onDelete('CASCADE');
            $table->softDeletes();

            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('senders');
    }
}
