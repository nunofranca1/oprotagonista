const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
//pagina de login

    .styles(
        [
            'resources/views/auth/front/vendor/bootstrap/css/bootstrap.css',
            'resources/views/auth/front/vendor/animate/animate.css',
            'resources/views/auth/front/vendor/css-hamburgers/hamburgers.css',
            'resources/views/auth/front/vendor/select2/select2.css',
        ], 'public/assets/auth/css/libs.css')
    .styles('resources/views/auth/front/css/util.css', 'public/assets/auth/css/util.css')
    .styles('resources/views/auth/front/css/main.css', 'public/assets/auth/css/main.css')
    .sass('resources/views/auth/front/font-awesome-4.7.0/scss/font-awesome.scss','public/assets/auth/css/font-awesome/font-awesome.css')
    .scripts(
        [
            'resources/views/auth/front/vendor/jquery/jquery-3.2.1.min.js',
            'resources/views/auth/front/vendor/bootstrap/js/popper.js',
            'resources/views/auth/front/vendor/bootstrap/js/bootstrap.js',
            'resources/views/auth/front/vendor/select2/select2.js',
            'resources/views/auth/front/vendor/tilt/tilt.jquery.min.js',
        ],'public/assets/auth/js/libs.js')
    .scripts('resources/views/auth/front/js/main.js', 'public/assets/auth/js/main.js')
    .copyDirectory('resources/views/auth/front/images', 'public/assets/auth/images')
    .copyDirectory('resources/views/auth/front/fonts', 'public/assets/auth/fonts')


    //site
   .js('resources/js/app.js', 'public/assets/site/js/app.js')
    .styles('resources/views/assets/css/style.css', 'public/assets/site/css/style.css')
    .styles(
        [
            'resources/views/assets/css/geral.css',
            'resources/views/assets/css/customizacao.css',
        ], 'public/assets/site/css/customizacao-geral.css')
    .styles('resources/views/assets/css/load_fonts.css', 'public/assets/site/css/load_fonts.css')
    .sass('resources/sass/app.scss', 'public/assets/site/css/app.css')
    .scripts(
        [
            // se nao descomentar ate dia 31 pode deletar
            //'resources/views/assets/js/geral.js',
            'resources/views/assets/js/script.js',
            //'resources/views/assets/js/custom.js',
        ], 'public/assets/site/js/script-geral-custom.js')
    .scripts(
        [
            'resources/views/assets/js/customizacao.js',
            'resources/views/front/assets/js/customizacao.js',
        ],'public/assets/site/js/customizacao.js')
    .scripts('resources/views/assets/js/style.js', 'public/assets/site/js/style.js')
    .scripts('resources/views/assets/js/libs/jquery.js', 'public/assets/site/js/libs/jquery.js')
    // .scripts('resources/views/assets/js/libs/jsmobile.js', 'public/assets/site/js/libs/jsmobile.js')
    .scripts('resources/views/assets/js/libs/ckEditor.js', 'public/assets/site/js/libs/ckEditor.js')
    .copyDirectory('resources/views/assets/img', 'public/assets/site/img')
    .copyDirectory('resources/views/assets/fonts', 'public/assets/site/fonts')

    //admin
    .scripts('resources/views/front/assets/js/customizacao.js', 'public/assets/admin/js/customizacao.js')
 

;
