<?php

namespace App\View\Components;

use App\Anuncio;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class AnunciosLaterais extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $anuncios_laterais;
    public function __construct()
    {
     Cache::remember('anuncios_laterais', 30, function () {
            return Anuncio::where('status', '1')->where('tamanho', '333x333')->inRandomOrder()->get();

        });
        $this->anuncios_laterais = Cache::get('anuncios_laterais');

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.anuncios-laterais');
    }
}
