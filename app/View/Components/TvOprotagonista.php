<?php

namespace App\View\Components;

use App\Movie;
use App\Post;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class TvOprotagonista extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $movies;
    public function __construct()
    {
        Cache::rememberForever('movieTv', function () {
            return Movie::take('1')
                ->orderByDesc('id')
                ->where('tv', '1')
                ->with('post')
                ->get();
        });
        $movies = Cache::get('movieTv');

        $this->movies = $movies;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.tv-oprotagonista');
    }
}
