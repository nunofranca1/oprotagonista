<?php

namespace App\View\Components;

use App\Anuncio;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class AnunciosTopo extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $anuncios_topo;
    public function __construct()
    {

        Cache::remember('anuncios_topo', 30, function () {
            return Anuncio::where('status', '1')->where('tamanho', '468x60')->take('4')->inRandomOrder()->get();

        });

        $this->anuncios_topo = Cache::get('anuncios_topo');

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.anuncios-topo');
    }
}
