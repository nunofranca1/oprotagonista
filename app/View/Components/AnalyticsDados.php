<?php

namespace App\View\Components;

use Illuminate\Support\Carbon;
use Illuminate\View\Component;
use Spatie\Analytics\AnalyticsFacade as Analytics;
use \Spatie\Analytics\Period;


class AnalyticsDados extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $visits30dias;
    public $visits7dias;
    public $ontem;
    public $pageMaisVista;


    public function __construct()
    {
        $this->visits30dias = Analytics::performQuery(Period::months(1), 'ga:pageViews');
        $this->visits7dias = Analytics::performQuery(Period::days('7'), 'ga:pageViews');

        $this->ontem = Analytics::performQuery(Period::create(Carbon::now()->subDay(), Carbon::now()->subDay()), 'ga:pageViews');


        $this->pageMaisVista = Analytics::fetchMostVisitedPages(Period::days('7'), $maxResults = 1);

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {


        return view('components.analytics-dados');
    }

}
