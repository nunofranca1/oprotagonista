<?php

namespace App\View\Components;

use App\Comment;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class Comments extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $comments;

    public function __construct($post)
    {
        Cache::rememberForever('comments' . $post, function () use ($post) {
            return $comments = Comment::where('post_id', $post)
                ->where('status', 1)
                ->orderByDesc('id')
                ->get();
        });

        $this->comments = Cache::get('comments' . $post);

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.comments');
    }
}
