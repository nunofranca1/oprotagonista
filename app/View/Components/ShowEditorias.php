<?php

namespace App\View\Components;

use App\Category;
use App\Post;
use Illuminate\View\Component;

class ShowEditorias extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $postPolicia;
    public $postPolitica;
    public $postCidade;
    public function __construct()
    {
       //teste
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.show-editorias');
    }
}
