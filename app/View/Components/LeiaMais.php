<?php

namespace App\View\Components;

use App\Post;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class LeiaMais extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $posts;
    public function __construct()
    {
        Cache::rememberForever('leia-mais', function () {
            return Post::orderByDesc('id')
                ->with(['imgs', 'category'])
                ->take('25')
                ->get();
        });
        $posts =  Cache::get('leia-mais')->filter(function ($value, $key) {
            return $key > 15 and $key <= 21;

        });

        $this->posts = $posts->all();

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.leia-mais');
    }
}
