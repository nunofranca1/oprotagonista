<?php

namespace App\View\Components;

use App\Post;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class MaisNoticias extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $posts;
    public function __construct()
    {
        Cache::rememberForever('mais_noticias', function () {
            return Post::orderByDesc('id')
                ->with(['imgs', 'category'])
                ->take('25')
                ->get();
        });

        $posts =  Cache::get('mais_noticias')->filter(function ($value, $key) {
            return $key > 6 and $key <= 15;

        });

        $this->posts = $posts->all();



    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.mais-noticias');
    }
}
