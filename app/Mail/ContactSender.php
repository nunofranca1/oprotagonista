<?php

namespace App\Mail;

use App\Sender;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactSender extends Mailable
{
    use Queueable, SerializesModels;
    private $sender;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Sender $sender)
    {
        $this->sender = $sender;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       $this->subject($this->sender->subject);
       $this->to('gutto23@hotmail.com', 'Augusto Ferreira');
       $this->replyTo($this->sender->contact->email, $this->sender->contact->name);

        return $this->markdown('page.mail.contact',[
            "mensagem" => $this->sender->mensagem,
            'sender' => $this->sender,
        ]);

    }
}
