<?php

namespace App\Console\Commands;

use App\Sender;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ContactSender extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contact:sender';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia email de contato para o adm do sistema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sender = Sender::where('status', '0')->with('contact')->first();

        if ($sender) {
            Mail::send(new \App\Mail\ContactSender($sender));

            $sender->status = '1';
            $sender->update();
        }


    }
}
