<?php

namespace App\Console\Commands;

use App\Http\Controllers\SiteController;
use App\Post;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UnScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UnScheduler:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publica post agendado';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        //retorna as postagens agendadas
        $date = Carbon::now();
        $data = $date->toArray();

        $agendadas = Post::where('scheduled', "1")->where('date_hour', '<=',  $data['formatted'])->get();
        if(count($agendadas) > '0'){
            foreach ($agendadas as $agendada) {
                $post = Post::find($agendada['id']);

                $post->scheduled = '0';
                $post->update();
            }
        }
    }
}
