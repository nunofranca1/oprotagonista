<?php

namespace App\Console\Commands;

use App\Post;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class CreateSiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Criar sitemap automaticamente para os mecanismos de pesquisas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // create new sitemap object
        $sitemap = App::make("sitemap");


        // get all posts from db
        $posts = Post::orderByDesc('date_hour')->where('scheduled', '0')->limit('100')->get();
        // add items to the sitemap (url, date, priority, freq)
        $sitemap->add("https://oprotagonistafsa.com.br/", $posts[0]->updated_at, '1.0', 'hourly');

        // add every post to the sitemap

        foreach ($posts as $post) {
            $sitemap->add("https://oprotagonistafsa.com.br/noticia/{$post->link}", $post->updated_at, "0.9", 'never');
        }

            // generate your sitemap (format, filename)
            $sitemap->store('xml', 'sitemap');

            // this will generate file mysitemap.xml to your public folder


    }
}
