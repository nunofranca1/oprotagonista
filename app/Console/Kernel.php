<?php

namespace App\Console;

use App\Console\Commands\ContactSender;
use App\Console\Commands\CreateSiteMap;
use App\Console\Commands\UnScheduler;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UnScheduler::class,
        ContactSender::class,
        CreateSiteMap::class

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // $schedule->command('inspire')->hourly();
        $schedule->command('UnScheduler:post')->everyMinute();
        $schedule->command('contact:sender')->everyMinute();
        $schedule->command('create:sitemap')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
