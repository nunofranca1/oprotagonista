<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAnunciosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'tamanho' => 'required',
            'img' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Informe um nome para o anúncio',
            'tamanho.required' => 'Informe o tamanho do anúncio para o sistema posiciona-lo',
            'img.required' => 'Envie um peça publicitária',
            'img.image' => 'Formato nao permitido. Envie um arquivo no formato jpeg,png,jpg ou gif',
            'img.mimes' => 'Formato nao permitido. Envie um arquivo no formato jpeg,png,jpg ou gif',
            'img.max' =>"Arquivo muito grande. Máximo de 2mb permitido"
        ];
    }
}
