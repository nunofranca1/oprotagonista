<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>   'required',
            'email' =>   'required|email',
//            'subject'=> 'required',
//            'mensagem'=> 'required',


        ];


    }
    public function message()
    {
        return [
            'name.required' => 'Seu nome é obrigatório',
            'email.required' => 'Seu e-mail é obrigatório',
            'email.email' => 'Formato do email não é válido',
            'subject.required' => 'O assunto é obrigatório',
            'mensagem.required' => 'Escreva sua mensagem',

        ];
    }
}
