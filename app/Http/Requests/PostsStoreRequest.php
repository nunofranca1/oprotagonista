<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'subtitle' => 'required',
            'content' => 'required',
            'tag' => 'required',
            'category_id' => 'required',
            'imagem' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'O título é obrigatório',
            'subtitle.required' => 'O subtítulo é obrigatório',
            'content.required' => 'Escreva um texto',
            'tag.required' => 'Informe ao menos uma tag',
            'category_id.required' => 'Escolha uma categoria'
        ];
    }
}
