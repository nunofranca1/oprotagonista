<?php

namespace App\Http\Controllers\Admin;

use App\HistoryTitle;
use App\Http\Requests\PostsStoreRequest;
use App\Img;
use App\Movie;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use Illuminate\Support\Str;
use App\Post;
use App\Category;
use \Intervention\Image\Facades\Image;
use Spatie\Analytics\AnalyticsFacade as Analytics;
use Spatie\Analytics\Period;


class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {

        $dadosPost = Analytics::fetchMostVisitedPages(Period::days(7), $maxResults = 100);
        $posts = Post::orderByDesc('date_hour')->with(['visities', 'comments'])->take(75)->simplePaginate(15);
        return view('page.admin.post.index',
            [
                'posts' => $posts,
                'dadosPost' => $dadosPost

            ]);
    }

    public function agendada()
    {
        //retorna as postagens agendadas

        $agendadas = Post::where('scheduled', "1")->get();


        return view('page.admin.post.agendada', [
            'agendadas' => $agendadas
        ]);
    }

    public function publicada()
    {
        //retorna as postagens agendadas

        $publicadas = Post::where('scheduled', "0")->get();


        return view('page.admin.post.publicada', [
            'publicadas' => $publicadas
        ]);
    }


    public function create()
    {


        $categorias = Category::all();

        return view('page.admin.post.create', [
            'categorias' => $categorias,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PostsStoreRequest $request)
    {

        $post = Post::where('link', Str::slug($request->title, "-"))->first();
        if ($post) {

            return redirect()->back()->with('error', 'Não é possivel criar o link dessa matéria. Troque o título');
        }

        $post = new Post();

        $post->title = $request->title;
        $post->subtitle = $request->subtitle;
        $post->tag = $request->tag;
        $post->content = $request->content;
        $post->category_id = $request->category_id;
        $post->date_hour = $request->date_hour;
        $post->link = Str::slug($request->title, "-");

        if (isset($request->date_hour) && !empty($request->date_hour)) {
            $post->date_hour = Carbon::parse($request->date_hour)->format('Y-m-d H:i');
            $post->scheduled = '1'; // campo que coloca postagem com agendada
        } else {
            $post->date_hour = Carbon::parse(date('Y-m-d H:i'))->format('Y-m-d H:i');
        }
        if ($post->save()) {
            Cache::forget('posts');

            if (!empty($request->link_movie)) {
                $link_video = explode("/", $request->link_movie);
                $movie = new Movie();
                $movie->link = $link_video[3];
                $movie->post_id = $post->id;
                $movie->save();
            }

        }

        if ($request->hasFile('imagem')) {

            foreach ($request->imagem as $imagem) {


                if ($imagem->isValid()) {

                    $image = new Img;

                    //-----salva o arquivo no disco *DESKTOP------//
                    $img_desktop_file = $imagem->store('desktop');

                    //atualiza tamanho e pixel da foto salva e subscreve
                    $img_desktop = Image::make($imagem)->save(storage_path('app/public/' . $img_desktop_file));

                    $img_desktop->fit(600, 450)->encode('jpg', '80');
                    $img_desktop->save();
                    //Salva o caminho para salvao no final no banco
                    $image->desktop = $img_desktop_file;


                    //-----salva o arquivo no disco *MOBILE------//


                    $img_mobile_file = $imagem->store('mobile');

                    //atualiza tamanho e pixel da foto salva e subscreve
                    $img_mobile = Image::make($imagem)->save(storage_path('app/public/' . $img_mobile_file));

                    $img_mobile->fit(600, 450)->encode('jpg', '80');
                    $img_mobile->save();
                    //Salva o caminho para salvao no final no banco
                    $image->mobile = $img_mobile_file;


                    //-----salva o arquivo no disco *MINIATURA------//

                    $img_miniatura_file = $imagem->store('miniaturas');

                    //atualiza tamanho e pixel da foto salva e subscreve
                    $img_miniatura = Image::make($imagem)->save(storage_path('app/public/' . $img_miniatura_file));

                    $img_miniatura->fit(600, 450)->encode('jpg', '80');
                    $img_miniatura->save();
                    //Salva o caminho para salvao no final no banco
                    $image->miniatura = $img_miniatura_file;


                    //SALVA TODOS OS CAMINHOS

                    $image->post_id = $post->id;

                    $image->save();
                    if (isset($image)) {
                        unset($image);
                    }

                    //fim cria miniatura


                }
            }
        }
        Cache::flush();
        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::with('movies')->find($id);
        $categorias = Category::all();

        if ($post) {
            return view('page.admin.post.edit', [
                'post' => $post,
                'categorias' => $categorias
            ]);
        } else {
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $post = Post::find($id);
        if ($request->title != $post->title) {
            $history_title = new HistoryTitle;
            $history_title->history = Str::slug($post->title);
            $history_title->post_id = $post->id;
            $history_title->save();
        }

        // salva a url da miniatura antiga para ser deletada quando a materia for editada
        $url_miniatura = $post->img;

        $post->title = $request->title;
        $post->subtitle = $request->subtitle;
        $post->tag = $request->tag;
        $post->content = $request->content;
        $post->category_id = $request->category_id;
        $post->link = Str::slug($request->title, "-");
        if ($request->hasFile('imagem')) {
            foreach ($request->imagem as $imagem) {
                if ($imagem->isValid()) {

                    $image = new Img;

                    //-----salva o arquivo no disco *DESKTOP------//
                    $img_desktop_file = $imagem->store('desktop');

                    //atualiza tamanho e pixel da foto salva e subscreve
                    $img_desktop = Image::make(public_path('storage/' . $img_desktop_file));
                    $img_desktop->fit(600, 450)->encode('jpg', '80');
                    $img_desktop->save();
                    //Salva o caminho para salvao no final no banco
                    $image->desktop = $img_desktop_file;


                    //-----salva o arquivo no disco *MOBILE------//

                    $img_mobile_file = $imagem->store('mobile');

                    //atualiza tamanho e pixel da foto salva e subscreve
                    $img_mobile = Image::make(public_path('storage/' . $img_mobile_file));
                    $img_mobile->fit(345, 400)->encode('jpg', '80');
                    $img_mobile->save();
                    //Salva o caminho para salvar no final no banco
                    $image->mobile = $img_mobile_file;


                    //-----salva o arquivo no disco *MINIATURA------//

                    $img_miniatura_file = $imagem->store('miniaturas');

                    //atualiza tamanho e pixel da foto salva e subscreve
                    $img_miniatura = Image::make(public_path('storage/' . $img_miniatura_file));
                    $img_miniatura->fit(250, 250)->encode('jpg', '80');
                    $img_miniatura->save();
                    //Salva o caminho para salvao no final no banco
                    $image->miniatura = $img_miniatura_file;

                    //SALVA TODOS OS CAMINHOS

                    $image->post_id = $post->id;

                    $image->save();
                    if (isset($image)) {
                        unset($image);
                    }

                    //fim cria miniatura
                }
            }
        }

        if (isset($request->date_hour) && !empty($request->date_hour)) {
            $post->date_hour = Carbon::parse($request->date_hour)->format('Y-m-d H:i');
            $post->scheduled = '1'; // campo que coloca postagem com agendada
        } else {
            $post->date_hour = Carbon::parse(date('Y-m-d H:i'))->format('Y-m-d H:i');
        }
        if ($post->update()) {

            if (!empty($request->link_movie)) {
                $movie = Movie::where('post_id', $post->id)->first();

                if ($movie) {
                    $movie->link = $request->link_movie;
                    $movie->post_id = $post->id;
                    $movie->update();
                    Cache::flush();
                    return redirect()->route('admin.index');
                } else {
                    $movie = new Movie();
                    $movie->link = $request->link_movie;
                    $movie->post_id = $post->id;

                    $movie->save();
                    Cache::flush();
                    return redirect()->route('admin.index');
                }

            } else {
                $movie = Movie::where('post_id', $post->id);
                $movie->delete();
                Cache::flush();
                return redirect()->route('admin.index');
            }
        }

    }
    public function destroy($id)
    {
        if ($id) {
            $post = Post::find($id);
            $post->delete();
            Cache::flush();
            return redirect()->route('admin.index');
        }
    }
}
