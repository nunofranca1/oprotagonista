<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactStoreRequest;
use App\Sender;


class ContactController extends Controller
{
    public function create()
    {
        return view('page.site.contact');
    }

    public function store(ContactStoreRequest $request)
    {

        $contact = Contact::where('email', $request->email)->count();

        if ($contact < 1) {
            $contato = new Contact;
            $contato->name = $request->name;
            $contato->email = $request->email;
            $contato->save();
        }else{
            $contato = Contact::where('email', $request->email)->first();
        }
        if(isset($request->subject))
        {
            $message = new Sender;
            $message->subject = $request->subject;
            $message->mensagem = $request->mensagem;
            $message->contact_id = $contato->id;
            $message->save();
        }

        toastr()->success('Mensagem enviado com sucesso');
        return redirect()->back();

    }
}
