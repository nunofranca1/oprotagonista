<?php

namespace App\Http\Controllers\Admin;

use App\Comment;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCommentValidate;
use App\Post;

use Illuminate\Support\Facades\Cache;


class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $comments = Comment::limit(15)->orderByDesc('id')->with('post')->paginate();
        return view('page.admin.comments.index',
            [
                'comments' => $comments
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCommentValidate $request, $link)
    {


        $post = Post::where('link', $link)->first();

        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->post_id = $post->id;
        if ($comment->save()) {
            toastr()->success('Comentário enviado com sucesso');
        } else {
            toastr()->error('Erro ao enviar comentário. Tente novamente');
        }
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

        $comment = Comment::find($id);
        if ($id) {
            if ($comment->status == "0") {
                $comment->status = '1';
            } else {
                $comment->status = '0';
            }

            $comment->update();
            Cache::flush();
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
