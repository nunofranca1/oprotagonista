<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAnunciosRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use App\Anuncio;
use Intervention\Image\Facades\Image;

class AnunciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {

    $anuncios = Anuncio::get();


        return view('page.admin.anuncio.index', [
                'anuncios' => $anuncios,

         ]);
    }

    public function create()
    {

        return view('page.admin.anuncio.create');
    }


    public function store(StoreAnunciosRequest $request)
    {
          $anuncio = $request->all();
            if($request->hasFile('img') && $request->img->isValid()){
                $image_path = $request->img->store('anuncios');

                $anuncio = new Anuncio();
                $anuncio->nome = $request->nome;
                $anuncio->tamanho = $request->tamanho;
                $anuncio->link = $request->link;
                $anuncio->img = $image_path;
            }

          if($anuncio->save()){
              Cache::forget('anuncios_laterais');
              Cache::forget('anuncios_topo');
           return redirect()->back()->with('success', 'Anúncio criado com sucesso');
          }else{
              return redirect()->back()->with('error', 'Erro ao criar anúncio');

          }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $anuncio= Anuncio::find($id);

        if($anuncio->status == 0){
           $anuncio['status'] = 1;

        }else{
            $anuncio['status'] = 0;

        }
        $anuncio->update();
        Cache::forget('anuncios_laterais');
        Cache::forget('anuncios_topo');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $anuncio = Anuncio::find($id);

        if($anuncio->delete()){
            if(Storage::exists("$anuncio->img")){
                Storage::delete("$anuncio->img");
            }
       }



        return redirect()->back();
    }
}
