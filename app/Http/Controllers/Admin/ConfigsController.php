<?php

namespace App\Http\Controllers\Admin;

use App\Config;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ConfigsController extends Controller
{

    public function configs()
    {
        return view('page.configs.configs');
    }

    public function store_configs(Request $request)
    {
            $config = new Config();
            $config->content = $request->content;
            $config->facebook = $request->facebook;
            $config->twitter = $request->twitter;
            $config->instagram = $request->instagram;
            $config->youtube = $request->youtube;
            $config->whatsapp = $request->whatstapp;

            if($config->save()){
                return redirect()->back()->with('success', 'Configurações atualizadas com sucesso');
            }else{
                return redirect()->back()->with('erro', 'Erro ao atualizar configurações');

            }
    }
}
