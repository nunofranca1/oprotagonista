<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMovieRequest;
use App\Movie;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies =Movie::orderByDesc('id')->paginate();
        return view('page.admin.movies.index',[
            'movies' =>$movies
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMovieRequest $request)
    {
       $link_movie = explode('/', $request->link);

       $movie = new Movie;
       $movie->title = $request->title;
       $movie->link = $link_movie[3];
       $movie->save();
       return redirect()->back()->with('success', 'Vídeo cadastrado com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        //Aqui eu pego eu busvo o video que foi clicado
        $movie = Movie::find($id);

        if($movie->tv =="1"){
            /*
             * Caso o video clicado esteja ativo ao clicar ele vai ser desativado.
             * o sitema faz uma busca em ordem decescente de id e ativa o primeiro video
             * para aparece na tv para nao ficar sem video. e desativa o video clicado
             *
             * */
            $movie->tv = "0";
            if($movie->update()){


                unset($movie);
                $movie = Movie::where('tv', '0')
                    ->where('id', '!=', $id)
                    ->orderByDesc('id')
                    ->first();

                $movie->tv = "1";
                $movie->update();
                unset($movie);
            }



        }else{
            $movie = Movie::where('tv', '1')->first();
            $movie->tv ='0';
            if($movie->update()){
                unset($movie);
                $movie = Movie::find($id);
                $movie->tv = "1";
                $movie->update();
                unset($movie);
            }


        }
        Cache::forget('movieTv');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movie =Movie::find($id);
        if($movie->delete()){
            // busca a materia desse video e atualiza o cache
            $post = Post::where('id', $movie->post->id)->first();
            Cache::forget('post' . $post->link);
            $movie = Movie::orderByDesc('id')->take('1')->first();
            $movie->tv = '1';
            $movie->save();
            Cache::forget('movieTv');
            return redirect()->back();

        }
    }
}
