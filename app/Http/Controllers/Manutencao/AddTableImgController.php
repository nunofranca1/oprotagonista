<?php

namespace App\Http\Controllers\Manutencao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddTableImgController extends Controller
{
    public
    function index()
    {


        // levar todas as fotos para tabela nova... deletar essa rota apos finalizar
        $posts = \App\Post::get();

        foreach ($posts as $post) {
            $img = new \App\Img();
            $img->post_id = $post->id;

            $desk = explode('/', $post->img);
            $img->desktop = "desktop/" . $desk['1'];

            $mob = explode('/', $post->img);
            $img->mobile = "mobile/" . $mob['1'];

            $min = explode('/', $post->img);
            $img->miniatura = "miniaturas/" . $min['1'];
            $img->save();
            unset($img);


        }
    }

}
