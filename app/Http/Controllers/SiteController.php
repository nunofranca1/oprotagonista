<?php

namespace App\Http\Controllers;


use App\Comment;
use App\HistoryTitle;
use App\Movie;
use App\View;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Cache;


class SiteController extends Controller
{

    public function index()
    {
        Cache::rememberForever('posts', function () {
            return Post::where('scheduled', '0')
                ->orderBy('created_at', 'DESC')
                ->with(['category', 'imgs'])
                ->limit(20)
                ->get();
        });
        $posts = Cache::get('posts');

        return view('page.site.index', [
            'posts' => $posts,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show(Post $post, $link)
    {

        if (!Cache::has('post' . $link)) {
            $postagem = Post::where('link', $link)->with(['comments', 'imgs', 'movies'])->first();
            if ($postagem) {
                Cache::rememberForever('post' . $link, function () use ($link) {
                    return Post::where('link', $link)->with(['comments', 'imgs', 'movies'])->first();;
                });
            }
            $history_tilte = HistoryTitle::where('history', $link)->first();
            if ($history_tilte) {
                Cache::rememberForever('post' . $link, function () use ($history_tilte) {
                    return Post::where('id', $history_tilte->post_id)->with(['comments','imgs', 'movies'])->first();
                });

            };
        }


        $postagem = Cache::get('post' . $link);
        if(is_null($postagem)){
            return redirect()->back();
        }
        return view('page/site/noticia', ['postagem' => $postagem]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {

        $search = $request->get('q');

        $noticias = Post::where('title', 'LIKE', '%' . $search . '%')
            ->orderByDesc('id')
            ->get();

        return view('page.site.search', [
            'noticias' => $noticias,
            'search' => $search
        ]);
    }

    public function blogger($ano, $mes, $materia)
    {

        return redirect("https://oprotagonistafsa.blogspot.com" . "/" . $ano . "/" . "$mes" . "/" . $materia);

    }
}
