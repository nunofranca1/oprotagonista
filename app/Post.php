<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $fillable = ['title', 'subtitle', 'content', 'tag', 'category_id', 'date_hour', 'img', 'scheduled', 'link'];

    public function category(){

        return $this->belongsTo(Category::class);
    }
    public function movies()
    {
        return $this->hasMany(Movie::class);
    }

    public function visities()
    {
        return $this->hasMany(View::class);
    }
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function imgs()
    {
        return $this->hasMany(Img::class)->orderByDesc('id');
    }

    public function historyTitle()
    {
        return $this->hasMany(HistoryTitle::class);
    }


}
