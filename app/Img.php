<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Img extends Model
{
    use SoftDeletes;
    public  function post()
    {
        return $this->belongsTo(Post::class);
    }
}
